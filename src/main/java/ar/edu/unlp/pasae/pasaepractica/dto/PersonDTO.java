package ar.edu.unlp.pasae.pasaepractica.dto;

import javax.validation.constraints.NotEmpty;

import ar.edu.unlp.pasae.pasaepractica.validations.annotations.PhoneNumberConstraint;

/**
 * Clase que representa al DTO de la Persona
 *
 * @author mbecca
 *
 */
public class PersonDTO {

	private Long id;
	@NotEmpty(message = "El nombre no puede ser nulo o vac�o")
	private String name;
	@NotEmpty(message = "El apellido no puede ser nulo o vac�o")
	private String surname;
	@PhoneNumberConstraint
	private String phoneNumber;

	public PersonDTO() {

	}

	public PersonDTO(final Long id, final String name, final String surname) {
		this();
		setId(id);
		setName(name);
		setSurname(surname);
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setSurname(final String surname) {
		this.surname = surname;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
