package ar.edu.unlp.pasae.pasaepractica.validations.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import ar.edu.unlp.pasae.pasaepractica.validations.PhoneNumberValidator;

@Documented
@Constraint(validatedBy = PhoneNumberValidator.class)
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface PhoneNumberConstraint {
	Class<?>[] groups() default {};

	String message() default "N�mero de tel�fono inv�lido";

	Class<? extends Payload>[] payload() default {};
}
