package ar.edu.unlp.pasae.pasaepractica.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import ar.edu.unlp.pasae.pasaepractica.entity.Auto;

/**
 * @author Mat�as Beccaria
 *
 */
public interface IAutoRepository extends ElasticsearchRepository<Auto, String> {

	List<Auto> findByPatente(String patente, Pageable pageable);
}