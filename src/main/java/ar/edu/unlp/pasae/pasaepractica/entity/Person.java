package ar.edu.unlp.pasae.pasaepractica.entity;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;

import ar.edu.unlp.pasae.pasaepractica.validations.annotations.PhoneNumberConstraint;

/**
 * Clase que representa a la entidad Persona
 *
 * @author mbecca
 *
 */
@Entity
public class Person extends AbstractEntity {

	@NotEmpty(message = "El nombre no puede ser nulo o vac�o")
	private String name;
	@NotEmpty(message = "El apellido no puede ser nulo o vac�o")
	private String surname;
	@PhoneNumberConstraint
	private String phoneNumber;

	private Person() {
	}

	public Person(final String name, final String surname) {
		this();
		setName(name);
		setSurname(surname);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Person other = (Person) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}

	public String getName() {
		return name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getSurname() {
		return surname;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (name == null ? 0 : name.hashCode());
		result = prime * result + (surname == null ? 0 : surname.hashCode());
		return result;
	}

	private void setName(final String name) {
		this.name = name;
	}

	private void setPhoneNumber(final String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	private void setSurname(final String surname) {
		this.surname = surname;
	}

	@Override
	public String toString() {
		return getName() + "-" + getSurname();
	}
}
