package ar.edu.unlp.pasae.pasaepractica.services;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unlp.pasae.pasaepractica.dto.PersonDTO;
import ar.edu.unlp.pasae.pasaepractica.dto.factories.IFactoryDTO;
import ar.edu.unlp.pasae.pasaepractica.entity.Person;
import ar.edu.unlp.pasae.pasaepractica.exceptions.BaseException;
import ar.edu.unlp.pasae.pasaepractica.exceptions.PersonNotFoundException;
import ar.edu.unlp.pasae.pasaepractica.repositories.IPersonRepository;
import ar.edu.unlp.pasae.pasaepractica.services.api.IPersonService;

/**
 * Clase que representa a la implemetación del servicio Persona
 *
 * @author mbecca
 *
 */
@Service
public class PersonService implements IPersonService {

	@Autowired
	private IPersonRepository personRepository;

	@Autowired
	private IFactoryDTO factoryDTO;

	@Autowired
	private Validator validator;

	@Override
	public void delete(final Long id) {
		getPersonRepository().deleteById(id);
	}

	@Override
	public PersonDTO findById(final Long id) {
		final Optional<Person> aOptionalPerson = getPersonRepository().findById(id);
		final Person aPerson = aOptionalPerson.get();
		return getFactoryDTO().convertToPersonDTO(aPerson);
	}

	@Override
	public PersonDTO findByNameAndSurname(final String name, final String surname) throws PersonNotFoundException {
		final Optional<Person> aOptionalPerson = getPersonRepository().findByNameAndSurname(name, surname);
		final Person aPerson = aOptionalPerson.orElseThrow(PersonNotFoundException::new);
		return getFactoryDTO().convertToPersonDTO(aPerson);
	}

	@Override
	public Collection<PersonDTO> findByNameStartingWithOrderByNameDesc(final String prefix)
			throws PersonNotFoundException {
		final List<Person> list = getPersonRepository().findByNameStartingWithOrderBySurnameDesc(prefix);
		return getFactoryDTO().convertToPersonDTOs(list);
	}

	private IFactoryDTO getFactoryDTO() {
		return factoryDTO;
	}

	private IPersonRepository getPersonRepository() {
		return personRepository;
	}

	@Override
	public Collection<PersonDTO> list() {
		final List<Person> persons = getPersonRepository().findAll();
		return getFactoryDTO().convertToPersonDTOs(persons);

	}

	@Override
	public void save(final String name, final String surname) {
		final Person aPerson = new Person(name, surname);
		final Person save = getPersonRepository().save(aPerson);

		final Set<ConstraintViolation<Person>> validations = validator.validate(save);
		validations.size();
		System.out.println(validations);
	}

	@Override
	public void thowException() throws BaseException {
		throw new RuntimeException("Excepción runtime");
	}

}
