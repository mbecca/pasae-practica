package ar.edu.unlp.pasae.pasaepractica.dto.factories;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

import org.springframework.stereotype.Component;

import ar.edu.unlp.pasae.pasaepractica.dto.AutoDTO;
import ar.edu.unlp.pasae.pasaepractica.dto.PersonDTO;
import ar.edu.unlp.pasae.pasaepractica.entity.Auto;
import ar.edu.unlp.pasae.pasaepractica.entity.Person;

/**
 * Clase que representa a la implementación de la fábrica de DTO's
 *
 * @author mbecca
 *
 */
@Component
public final class FactoryDTO implements IFactoryDTO {

	private static <A, R> Collection<R> createCollections(final Collection<A> col, final Function<A, R> function,
			final Collection<R> result) {
		col.stream().map(s -> function.apply(s)).forEach(result::add);
		return result;
	}

	@Override
	public AutoDTO convertToAutoDTO(final Auto auto) {
		return new AutoDTO(auto.getId(), auto.getPatente(), auto.getMarca(), auto.getModelo(), auto.getColor());
	}

	@Override
	public Collection<AutoDTO> convertToAutoDTOs(final List<Auto> autos) {
		return createCollections(autos, this::convertToAutoDTO, new ArrayList<>());
	}

	@Override
	public PersonDTO convertToPersonDTO(final Person aPerson) {
		return new PersonDTO(aPerson.getId(), aPerson.getName(), aPerson.getSurname());
	}

	@Override
	public Collection<PersonDTO> convertToPersonDTOs(final List<Person> persons) {
		return createCollections(persons, this::convertToPersonDTO, new ArrayList<>());
	}

}
